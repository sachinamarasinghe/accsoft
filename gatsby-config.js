const guid = process.env.NETLIFY_GOOGLE_ANALYTICS_ID;

module.exports = {
  siteMetadata: {
    title: "Active Digital Labs",
    description: "Copyright by Active Digital Labs",
    contact: {
      phone: "XXX XXX XXX",
      email: "zerostaticthemes@gmail.com",
    },
    menuLinks: [
      {
        name: "Who We Are",
        link: "#who-we-are",
        linkClass: "",
      },
      {
        name: "What We Offer",
        link: "#what-we-offer",
        linkClass: "",
      },
      {
        name: "Featured Projects",
        link: "#featured-products",
        linkClass: "",
      },
      {
        name: "Our Pricing",
        link: "#our-pricing",
        linkClass: "",
      },
      {
        name: "Our Clients",
        link: "#our-clients",
        linkClass: "",
      },
      {
        name: "Contact Us",
        link: "#contact",
        linkClass: "button-light",
      },
    ],
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-transformer-json",
    "gatsby-transformer-remark",
    "gatsby-plugin-react-helmet",
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: `${__dirname}/src/pages`,
        name: "pages",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: `${__dirname}/src/data`,
        name: "data",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: `${__dirname}/src/images`,
        name: "images",
      },
    },
    {
      resolve: "gatsby-plugin-google-analytics",
      options: {
        trackingId: guid || "UA-XXX-1",
        // Puts tracking script in the head instead of the body
        head: false,
      },
    },
    // {
    //   resolve: 'gatsby-plugin-prefetch-google-fonts',
    //   options: {
    //     fonts: [
    //       {
    //         family: 'Montserrat',
    //         variants: ['300', '400', '500', '600', '700', '800'],
    //       },
    //     ],
    //   },
    // },
    "gatsby-plugin-modal-routing",
    // {
    //   resolve: "gatsby-plugin-modal-routing",
    //   options: {
    //     // A selector to set react-modal's app root to, default is `#___gatsby`
    //     // See http://reactcommunity.org/react-modal/accessibility/#app-element
    //     appElement: "#___gatsby",

    //     // Object of props that will be passed to the react-modal container
    //     // See http://reactcommunity.org/react-modal/#usage
    //     modalProps: {},
    //   },
    // },
  ],
};
