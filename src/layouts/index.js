import React from "react";
import SEO from "../components/SEO";
import Header from "../components/Header";

const Layout = (props) => (
  <React.Fragment>
    <SEO />
    <div
      className={`page${props.bodyClass ? ` ${props.bodyClass}` : ""}`}
      id={props.bodyId}
    >
      <div id="wrapper" className="wrapper">
        <Header />
        {props.children}
      </div>
      {/* <Footer />
        <SubFooter /> */}
    </div>
  </React.Fragment>
);

export default Layout;
