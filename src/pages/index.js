import React, { Component } from "react";
import ReactModal from "react-modal";
import Helmet from "react-helmet";
import SEO from "../components/SEO";
import Layout from "../layouts/index";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import AboutUs from "../components/Index/about-us";
import WhoWeAre from "../components/Index/who-we-are";
import WhatWeOffer from "../components/Index/what-we-offer";
import FeaturedProjects from "../components/Index/featured-projects";
import OurClients from "../components/Index/our-clients";
import Contact from "../components/Index/contact";
import Pricing from "../components/Index/pricing";
import ScrollToTop from "react-scroll-to-top";
import Icon from "@mdi/react";
import PricingSection from "../components/Index/pricing-section";
import { mdiClose } from "@mdi/js";

// icons ----------------------------------------
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

ReactModal.setAppElement("#___gatsby");
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
    };
    this.modalOpenInitialy();
  }

  handleModalOpen = () => {
    this.setState({ isModalOpen: true });
  };

  handleModalClose = () => {
    this.setState({ isModalOpen: false });
  };

  modalOpenInitialy = () => {
    setTimeout(() => this.setState({ isModalOpen: true }), 3000);
  };

  render() {
    return (
      <Layout bodyClass="page-home" bodyId="home">
        <SEO />
        <Helmet>
          <meta name="description" content="AccSoft" />
        </Helmet>

        {/* about us ----------------------------  */}
        <AboutUs />
        {/* who we are------------------------------------  */}
        <WhoWeAre />
        {/* what we offer ----------------------------------- */}
        <WhatWeOffer />
        {/* featured projects ---------------------------------------- */}
        <FeaturedProjects />
        {/* pricing -----------------------------------  */}
        <PricingSection />
        {/* our clients and expertise -----------------------------------  */}
        <OurClients />
        {/* contact us ---------------------------------------------------  */}
        <Contact />
        {/* modal  */}
        <ReactModal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.handleModalClose}
          className="price-modal"
        >
          <Pricing />
          <button onClick={this.handleModalClose} className="modal-close">
            <Icon path={mdiClose} size={2} color="#000000" />
          </button>
        </ReactModal>
        <ScrollToTop smooth className="scroll-to-top" />
      </Layout>
    );
  }
}

export default Home;
