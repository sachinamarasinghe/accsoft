import React from "react";
import features from "../../data/features";
import { StaticImage } from "gatsby-plugin-image";
const obj = features[2];

const UIDesign = () => (
  <div className="service-modal">
    <div className="service-modal--header">
      <h2>What We Offer</h2>
    </div>

    <div className="service-modal--body">
      <div className="left">
        {/* <img src={obj.image} className="service-img" /> */}
        <StaticImage
          src="../../images/what-we-offer/ui-ux.jpg"
          alt="ui ux"
          loading="eager"
          placeholder="dominantColor"
        />
      </div>
      <div className="right">
        <h5 className="sub-header">{obj.title}</h5>
        <p className="content-text">{obj.description}</p>
      </div>
    </div>
    {/* <div className="service-modal--footer">
          <Link to="/">Go back to the homepage</Link>
        </div> */}
  </div>
);

export default UIDesign;
