import React from "react";
import features from "../../data/features";
const obj = features[1];
import { StaticImage } from "gatsby-plugin-image";

const MobileDev = () => (
  <div className="service-modal">
    <div className="service-modal--header">
      <h2>What We Offer</h2>
    </div>

    <div className="service-modal--body">
      <div className="left">
        <StaticImage
          src="../../images/what-we-offer/mobiledev.jpg"
          alt="mobile dev"
          loading="eager"
          placeholder="dominantColor"
        />
      </div>
      <div className="right">
        <h5 className="sub-header">{obj.title}</h5>
        <p className="content-text">{obj.description}</p>
        <ul className="service-ul">
          <li>Native application development</li>
          <li>Hybrid application development</li>
          <li>Game development</li>
        </ul>
      </div>
    </div>
    {/* <div className="service-modal--footer">
          <Link to="/">Go back to the homepage</Link>
        </div> */}
  </div>
);

export default MobileDev;
