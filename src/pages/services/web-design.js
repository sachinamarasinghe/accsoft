import React from "react";
import features from "../../data/features";
import { StaticImage } from "gatsby-plugin-image";
const obj = features[0];

const WebDesign = () => (
  <div className="service-modal">
    <div className="service-modal--header">
      <h2>What We Offer</h2>
    </div>

    <div className="service-modal--body">
      <div className="left">
        {/* <img src={obj.image} className="service-img" /> */}
        <StaticImage
          src="../../images/what-we-offer/webdev.jpg"
          alt="web dev"
          loading="eager"
          placeholder="dominantColor"
        />
      </div>
      <div className="right">
        <h5 className="sub-header">{obj.title}</h5>
        <p className="content-text">{obj.description}</p>
        <ul className="service-ul">
          <li>Personalized web development</li>
          <li>Business web site</li>
          <li>E-Commerce &amp; Retail</li>
          <li>Web portal</li>
        </ul>
      </div>
    </div>
    {/* <div className="service-modal--footer">
          <Link to="/">Go back to the homepage</Link>
        </div> */}
  </div>
);

export default WebDesign;
