import React from "react";
import Slider from "react-slick";
import activetracker from "../../images/activetracker.png";
import activecar from "../../images/activecar.png";
import afslogo from "../../images/afslogo.png";
import mslogo from "../../images/mslogo.png";
import TAndT_logo from "../../images/t&t_logo.png";
import zero_consulting_logo from "../../images/zero_consulting_logo.png";
import astrovidhi_logo from "../../images/astrovidhi_logo.png";
import yamios_logo from "../../images/yamios_logo.jpg";
import speedy_stamps_logo from "../../images/speedy_stamps_logo.png";

const FeaturedProjects = () => {
  return (
    <div className="featured-products" id="featured-products">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h2 className="text-center">Featured Projects</h2>
            <h5 className="text-center mb-4">Things we have made</h5>
          </div>
          <div className="col-12 col-6 project-cards">
            <Slider
              {...{
                accessibility: false,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 2,
                responsive: [
                  {
                    breakpoint: 1200,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    },
                  },
                  {
                    breakpoint: 991,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                    },
                  },
                ],
              }}
            >
              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${activetracker})` }}
                />
                <h5>Active Tap</h5>
                <h6>Mobile Application</h6>
              </div>
              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${yamios_logo})` }}
                />
                <h5>Yamios Pizza</h5>
                <a
                  href="https://www.yamiospizza.com.au"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>

              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${TAndT_logo})` }}
                />
                <h5>T &#38; T</h5>
                <a
                  href="https://www.tandtgroup.in/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>

              <div className="project-card">
                <div
                  className="img"
                  style={{
                    backgroundImage: `url(${zero_consulting_logo})`,
                  }}
                />
                <h5>Zero Consulting</h5>
                <a
                  href="https://www.zeroconsulting.com/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>

              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${activecar})` }}
                />
                <h5>Active Car Care</h5>
                <a
                  href="https://www.activecarcare.com.au/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>

              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${astrovidhi_logo})` }}
                />
                <h5>Astrovidhi</h5>
                <a
                  href="https://www.astrovidhi.com/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>

              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${mslogo})` }}
                />
                <h5>Melbourne Sanitisers</h5>
                <a
                  href="https://melbournesanitisers.com.au/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>

              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${afslogo})` }}
                />
                <h5>Active Facility Services</h5>
                <a
                  href="https://www.activefacilityservices.com.au/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>

              <div className="project-card">
                <div
                  className="img"
                  style={{ backgroundImage: `url(${speedy_stamps_logo})` }}
                />
                <h5>Speedy Stamps</h5>
                <a
                  href="https://speedystamps.com.au/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  View Website
                </a>
              </div>
            </Slider>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeaturedProjects;
