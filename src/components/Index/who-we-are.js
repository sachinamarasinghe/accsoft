import React from "react";
import whoWeAre from "../../images/who-we-are.jpg";

const WhoWeAre = () => {
  return (
    <div
      className="who-we-are"
      style={{ backgroundImage: `url(${whoWeAre})` }}
      id="who-we-are"
    >
      <div className="container">
        <div className="row justify-content-end">
          <div className="col-lg-6 col-md-12">
            <div className="content">
              <h2>Who We Are</h2>
              <h5>We are more than a digital agency</h5>
              <p>
                Active Digital Labs providing a complete suite of online
                solutions with well experienced experts. We are the best to
                create websites and mobile applications in Australia and Sri
                Lanka.
              </p>
              <p>
                As our team is professional, experienced and forward thinking
                and has a big strength in translating workable solutions from
                business requirements this was observed from the clients.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WhoWeAre;
