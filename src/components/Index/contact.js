import React from "react";
import Icon from "@mdi/react";
import {
  mdiPhoneOutline,
  mdiMapMarkerCircle,
  mdiEmailOutline,
  mdiFacebook,
  mdiLinkedin,
  mdiInstagram,
} from "@mdi/js";
import map from "../../images/map.png";

const Contact = () => {
  return (
    <div className="contact" id="contact">
      <div className="container mt-5">
        <div className="row">
          <div className="col-lg-6 col-md-12 message">
            <h2 className="text-left">Send Us a Message</h2>
            <form>
              <div className="form-group">
                <label htmlFor="name">Your name</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  aria-describedby="name"
                  placeholder="Enter Name"
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">E-mail Address</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  placeholder="Enter E-mail"
                />
              </div>
              <div className="form-group">
                <label htmlFor="phone">Phone Number</label>
                <input
                  type="tel"
                  className="form-control"
                  id="phone"
                  placeholder="Enter phone number"
                />
              </div>
              <div className="form-group">
                <label htmlFor="message">Message</label>
                <textarea
                  type="text"
                  className="form-control"
                  id="message"
                  placeholder="Enter your message"
                />
              </div>

              <button type="submit" className="button secondary mt-2">
                Send Message
                <span />
              </button>
            </form>
          </div>
          <div className="col-lg-6 col-md-12 contact-us">
            <h2 className="text-left">Contact Us</h2>
            <p>We would love to hear from you</p>
            <img src={map} className="map" />
            <div className="d-flex w-100 addresses">
              <div className="d-flex pr-3">
                <Icon path={mdiMapMarkerCircle} size={1.5} />
                <div className="address">
                  <h6>Australian Branch</h6>
                  <p>
                    6 Stewart Street, <br />
                    Burwood, <br />
                    VIC 3125, <br />
                    Australia
                  </p>
                </div>
              </div>
              <div className="d-flex">
                <Icon path={mdiMapMarkerCircle} size={1.5} />
                <div className="address">
                  <h6>Sri Lankan Branch</h6>
                  <p>
                    304/2A Wawanawatta Road, <br />
                    Piliyandala 10300, <br />
                    Sri Lanka
                  </p>
                </div>
              </div>
            </div>
            <div className="tel-email">
              <h6>
                <Icon path={mdiPhoneOutline} size={1} />
                <a href="tel:1300 37 37 90">1300 37 37 90</a>
              </h6>

              <h6>
                <Icon path={mdiEmailOutline} size={1} />
                <a href="mailto:sales@activedl.com">sales@activedl.com</a>
              </h6>
            </div>
            <div className="social-media">
              <h4>Follow Us</h4>
              <div className="d-flex">
                <a
                  href="https://www.facebook.com/activedigitallabs"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <Icon path={mdiFacebook} color="#84CF3E" />
                </a>
                <a
                  href="https://www.linkedin.com/company/42712358/admin"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <Icon path={mdiLinkedin} color="#84CF3E" />
                </a>
                <a
                  href="https://www.instagram.com/active_digital_labs"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <Icon path={mdiInstagram} color="#84CF3E" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
