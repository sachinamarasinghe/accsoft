import React from "react";
import whatweofferbg from "../../images/whatweoffer-bg.png";
import webDesign from "../../images/what-we-offer/computer.svg";
import mobileAppDev from "../../images/what-we-offer/smartphone.svg";
import uiUx from "../../images/what-we-offer/idea.svg";
import Modal from "react-modal";
import WebDesign from "../../pages/services/web-design";
import MobileDev from "../../pages/services/mobile-dev";
import UIDesign from "../../pages/services/ui-design";
import Icon from "@mdi/react";
import { mdiClose } from "@mdi/js";

Modal.setAppElement("#___gatsby");
const WhatWeOffer = () => {
  const [modalIsOpen, setIsOpen] = React.useState(false);
  const [modalIsOpen2, setIsOpen2] = React.useState(false);
  const [modalIsOpen3, setIsOpen3] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }
  function openModal2() {
    setIsOpen2(true);
  }
  function openModal3() {
    setIsOpen3(true);
  }

  function closeModal() {
    setIsOpen(false);
    setIsOpen2(false);
    setIsOpen3(false);
  }
  return (
    <div
      className="what-we-offer"
      style={{ backgroundImage: `url(${whatweofferbg})` }}
      id="what-we-offer"
    >
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h2 className="text-center mt-3">What We Offer</h2>
            {/* <h5 className="text-center mb-4">We work with you, Not for you</h5> */}
            <h5 className="text-center mb-4">
              Active Digital Labs glad to share the following services which are
              based on client satisfaction at the acceptable cost
            </h5>
          </div>
          <div className="col-12 offer-cards">
            <div className="offer-card">
              <a onClick={openModal} className="offer-card-wrapper">
                <div className="img-wrapper">
                  <img src={webDesign} />
                </div>
                <p>Web Design and Development</p>
                <a onClick={openModal}>Learn More</a>
              </a>
            </div>
            <div className="offer-card">
              <a onClick={openModal2} className="offer-card-wrapper">
                <div className="img-wrapper">
                  <img src={mobileAppDev} />
                </div>
                <p>Mobile App Development</p>
                <a onClick={openModal2}>Learn More</a>
              </a>
            </div>
            <div className="offer-card">
              <a onClick={openModal3} className="offer-card-wrapper">
                <div className="img-wrapper">
                  <img src={uiUx} />
                </div>
                <p>
                  UI/UX
                  <br />
                  Solutions
                </p>
                <a onClick={openModal3}>Learn More</a>
              </a>
            </div>
          </div>
        </div>
      </div>

      {/* modals  */}
      <Modal isOpen={modalIsOpen} onRequestClose={closeModal}>
        <button onClick={closeModal} className="close">
          <Icon path={mdiClose} size={1} color="#919191" />
        </button>
        <WebDesign />
      </Modal>
      <Modal isOpen={modalIsOpen2} onRequestClose={closeModal}>
        <button onClick={closeModal} className="close">
          <Icon path={mdiClose} size={1} color="#919191" />
        </button>
        <MobileDev />
      </Modal>
      <Modal isOpen={modalIsOpen3} onRequestClose={closeModal}>
        <button onClick={closeModal} className="close">
          <Icon path={mdiClose} size={1} color="#919191" />
        </button>
        <UIDesign />
      </Modal>
    </div>
  );
};

export default WhatWeOffer;
