import React from "react";
import ReactTooltip from "react-tooltip";
import plans from "../../data/plans";
const Pricing = () => {
  return (
    <div className="pricing-content">
      <ReactTooltip
        class="tool-tip"
        id="general"
        getContent={(message) => message}
        place="right"
        backgroundColor="rgba(0,0,0,0.9)"
      />
      <div className="row">
        <div className="col-lg-4 col-md-12 col-sm-12 plan ">
          <div className="heading">
            <h2>{plans.plan_type.lite_plan.title}</h2>
            <p>{plans.plan_type.lite_plan.description}</p>
          </div>
          {/* <div className="price">$ 120</div> */}
          <div className="content">
            <label>
              {plans.plan_details.responsive.title}
              <i
                data-tip={plans.plan_details.responsive.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>{plans.plan_details.bandwidth.title}</label>
            <label>
              {plans.plan_details.seo.title}
              <i
                data-tip={plans.plan_details.seo.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>{plans.plan_details.chat.title}</label>
            <label>{plans.plan_details.support.title}</label>
            <label>
              {plans.plan_details.domain.title}
              <i
                data-tip={plans.plan_details.domain.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>1 {plans.plan_details.hosting.title}</label>
          </div>
        </div>
        <div className="col-lg-4 col-md-12 col-sm-12 plan">
          <div className="heading">
            <div className="banner">Recomended</div>
            <h2>{plans.plan_type.company_plan.title}</h2>
            <p>{plans.plan_type.company_plan.description}</p>
          </div>
          {/* <div className="price">$ 120</div> */}
          <div className="content">
            <label>{plans.plan_details.logo_design.title}</label>
            <label>
              {plans.plan_details.responsive.title}
              <i
                data-tip={plans.plan_details.responsive.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>{plans.plan_details.bandwidth.title}</label>
            <label>
              {plans.plan_details.seo.title}
              <i
                data-tip={plans.plan_details.seo.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>{plans.plan_details.chat.title}</label>
            <label>{plans.plan_details.support.title}</label>
            <label>
              {plans.plan_details.domain.title}
              <i
                data-tip={plans.plan_details.domain.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>5{plans.plan_details.hosting.title}</label>
          </div>
        </div>
        <div className="col-lg-4 col-md-12 col-sm-12 plan">
          <div className="heading">
            <h2>{plans.plan_type.business_plan.title}</h2>
            <p>{plans.plan_type.business_plan.description}</p>
          </div>
          {/* <div className="price">$ 120</div> */}
          <div className="content">
            <label>{plans.plan_details.logo_design.title}</label>
            <label>
              {plans.plan_details.responsive.title}
              <i
                data-tip={plans.plan_details.responsive.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>{plans.plan_details.bandwidth.title}</label>
            <label>{plans.plan_details.cms.title}</label>
            <label>
              {plans.plan_details.payment.title}
              <i
                data-tip={plans.plan_details.payment.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>
              {plans.plan_details.seo.title}
              <i
                data-tip={plans.plan_details.seo.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>{plans.plan_details.chat.title}</label>
            <label>{plans.plan_details.social_media.title}</label>
            <label>{plans.plan_details.support.title}</label>
            <label>
              {plans.plan_details.domain.title}
              <i
                data-tip={plans.plan_details.domain.description}
                className="info"
                data-for="general"
              >
                !
              </i>
            </label>
            <label>
              10
              {plans.plan_details.hosting.title}
            </label>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Pricing;
