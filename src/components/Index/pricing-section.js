import React from "react";
import Pricing from "./pricing";

const PricingSection = () => {
  return (
    <section className="pricing-section" id="our-pricing">
      <h2 className="text-center">Our Pricing</h2>
      <h5 className="text-center mb-4">
        Active Digital Labs have a price plan catered for your need
      </h5>
      <div className="container">
        <Pricing />
      </div>
    </section>
  );
};

export default PricingSection;
