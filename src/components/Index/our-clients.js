import React from "react";
import flutter from "../../images/logos/flutter.png";
import swift from "../../images/logos/swift.png";
import xamarin from "../../images/logos/xamarin.png";
import android from "../../images/logos/android.png";
import magento from "../../images/logos/magento.png";
import node from "../../images/logos/node.png";
import react from "../../images/logos/react.png";
import shopify from "../../images/logos/shopify.png";
import wordpress from "../../images/logos/wordpress.png";
import amazon from "../../images/logos/amazon.png";
import client1 from "../../images/logos/motorsports_gray.png";
import client2 from "../../images/logos/ms_gray.png";
import client3 from "../../images/logos/chad_gray.png";
import client4 from "../../images/logos/t&t_gray.png";
import client5 from "../../images/logos/carcare_gray.png";
import client6 from "../../images/logos/yamios.png";

const OurClients = () => {
  return (
    <div className="container our-clients" id="our-clients">
      <div className="row">
        <div className="col-lg-6 col-md-12 expertise">
          <div className="row">
            <div className="col-12">
              <h2 className="text-center">Our Expertise</h2>
              <h5 className="text-center mb-4">Our Technology stack</h5>
            </div>
            <div className="col-12">
              <div className="logos">
                <img src={xamarin} />
                <img src={flutter} />
                <img src={android} />
                <img src={swift} />
                <img src={magento} />
                <img src={node} />
                <img src={react} />
                <img src={shopify} />
                <img src={wordpress} />
                <img src={amazon} />
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-12 clients">
          <div className="row">
            <div className="col-12">
              <h2 className="text-center">Our Clients</h2>
              <h5 className="text-center mb-4">
                We value our relationship with you
              </h5>
            </div>
            <div className="col-12">
              <div className="logos">
                <img src={client1} />
                <img src={client2} />
                <img src={client3} />
                <img src={client4} />
                <img src={client5} />
                <img src={client6} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurClients;
