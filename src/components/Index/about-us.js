import { Link } from "gatsby";
import React from "react";
import img1 from "../../images/1.jpg";
import ReactWOW from "react-wow";

const AboutUs = () => {
  return (
    <div className="about-us" style={{ backgroundImage: `url(${img1})` }}>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className=" content">
              <ReactWOW delay=".1s" animation="fadeInLeft">
                <h1>
                  We're Digital Solutions
                  <br />
                  Design &amp; Development
                  <br /> Agency
                </h1>
              </ReactWOW>
              <ReactWOW delay=".11s" animation="fadeInLeft">
                <h5>Focused on Creative and Best Quality Solutions.​</h5>
              </ReactWOW>
              <ReactWOW delay=".12s" animation="fadeInLeft">
                <Link
                  className="button primary mt-2"
                  style={{ textDecoration: "none" }}
                  to="#who-we-are"
                >
                  Learn More
                  <span />
                </Link>
              </ReactWOW>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;
