import React from 'react';
import { Link, ModalRoutingContext } from 'gatsby-plugin-modal-routing';
import Layout from '../layouts/index';

const ModalContent = ({ children, ...rest }) => (
  <ModalRoutingContext.Consumer>
    {({ modal, closeTo }) => (modal ? (
        <React.Fragment>
          <Link className="close-icon" to={closeTo}>
            <i className="lni-close" />
          </Link>
          {children}
          <Link className="close-link" to={closeTo}>
            Close
          </Link>
        </React.Fragment>
    ) : (
        <Layout {...rest}>{children}</Layout>
    ))
    }
  </ModalRoutingContext.Consumer>
);

export default ModalContent;
