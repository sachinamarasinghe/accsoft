import React, { Component } from 'react';
import ReactModal from 'react-modal';
import Layout from '../layouts';

class Banner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: true,
    };
  }

  handleModalOpen = (event) => {
    // console.log('handleModalOpen: ', event);
    this.setState({ isModalOpen: true });
  };

  handleModalClose = (event) => {
    // console.log('handleModalOpen: ', event);
    this.setState({ isModalOpen: false });
  };

  render() {
    return (
      <Layout>
        {/* modal  */}
        <ReactModal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.handleModalClose}
          contentLabel="Example Modal In Gatsby"
          className="price-modal"
          //   style={customStyles}
        >
          <h2>Donate</h2>
          <button onClick={this.handleModalClose}>Close Modal</button>
        </ReactModal>
      </Layout>
    );
  }
}
export default Banner;
