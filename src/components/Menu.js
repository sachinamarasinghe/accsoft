import React, { useEffect } from "react";
import { graphql, StaticQuery, Link as GatsbyLink } from "gatsby";
import { Link as ScrollLink, scroller, Events, scrollSpy } from "react-scroll";
import ScrollIntoView from "react-scroll-into-view";

const Menu = (props) => {
  const { menuLinks } = props.data.site.siteMetadata;

  useEffect(() => {
    Events.scrollEvent.register("begin", function(to, element) {
      console.log("begin", arguments);
    });

    Events.scrollEvent.register("end", function(to, element) {
      console.log("end", arguments);
    });

    scrollSpy.update();
  });
  return (
    <div id="main-menu" className="main-menu">
      <ul>
        {/* {menuLinks.map((link) => (
          <li key={link.name}>
            <GatsbyLink to={link.link} className={link.linkClass}>
              {link.name}
            </GatsbyLink>
          </li>
        ))} */}
        {menuLinks.map((link) => (
          <li key={link.name}>
            <a>
              <ScrollIntoView selector={link.link} alignToTop={true}>
              {link.name}
            </ScrollIntoView>
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default (props) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            menuLinks {
              name
              link
              linkClass
            }
          }
        }
      }
    `}
    render={(data) => <Menu data={data} />}
  />
);
