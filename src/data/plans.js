const plans = {
  currency_type: 'AUD ',
  plan_type: {
    lite_plan: {
      title: 'Lite Plan',
      description: 'Single page website (Best for personalized web site)',
      pricing: '',
    },
    company_plan: {
      title: 'Company Plan',
      description: '8 – 12 static pages website (Best for you company)',
      pricing: '',
    },
    business_plan: {
      title: 'Business Plan',
      description: 'Dynamic E – commerce website (Online shopping)',
      pricing: '',
    },
  },
  plan_details: {
    logo_design: {
      title: 'Logo design-up to 5 revisions',
      description: '',
    },
    responsive: {
      title: 'Responsive Website !',
      description: 'Website will properly load for desktop, laptops, mobiles and tablets',
    },
    bandwidth: {
      title: 'Unlimited Bandwidth',
      description: '',
    },
    cms: {
      title: 'Content Management System',
      description: '',
    },
    payment: {
      title: 'Online payment',
      description: 'You can receive online payments through IPG, paypal, etc',
    },
    seo: {
      title: 'Search engine optimization(SEO)',
      description: 'Maximizing the visitors of website',
    },
    chat: {
      title: 'Live chat integration',
      description: '',
    },
    social_media: {
      title: 'Social media integration',
      description: '',
    },
    support: {
      title: '24/7 Phone and email support',
      description: '',
    },
    domain: {
      title: '1 Year Domain',
      description: '.com or .com.au domain. customer can reserve additional domain name by paying extra amount',
    },
    hosting: {
      title: ' GB Hosting Space',
      description: '',
    },
  },
};

export default plans;
