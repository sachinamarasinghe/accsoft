import webdev from '../images/what-we-offer/webdev.jpg';
import ui from '../images/what-we-offer/ui-ux.jpg';
import mobile from '../images/what-we-offer/mobiledev.jpg';

const features = [
  {
    title: 'Web Design and Development',
    description: 'Increase the profitability, availability, and efficiency of your business via the relevant web development solutions with the scalable architecture using the latest technologies and trends.',
    image: webdev,
  },
  {
    title: 'Mobile App Development',
    description: 'We combine extensive product development expertise with great designs and proven processes to build high quality mobile applications. We mostly use technology as Swift, Java, React Native and Flutter.',
    image: mobile,
  },
  {
    title: 'UI/UX Solutions',
    description: 'Our UI/UX team works closely with a design team based in the Sri Lanka to design and develop the professional, polished user interfaces of our solutions.',
    image: ui,
  },
];

export default features;
